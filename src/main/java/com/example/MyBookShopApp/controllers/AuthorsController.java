package com.example.MyBookShopApp.controllers;

import com.example.MyBookShopApp.data.AuthorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/authors")//http://localhost:8085/authors/index.html
public class AuthorsController {

    AuthorService authorService;
    @Autowired
    public AuthorsController(AuthorService authorService) {
        this.authorService = authorService;
    }

    @GetMapping("")
    public String authorsPage(Model model){
        model.addAttribute("authors", authorService.getAuthorData());
        return "authors/index";
    }

}
