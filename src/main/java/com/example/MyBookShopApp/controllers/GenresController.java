package com.example.MyBookShopApp.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/genres") //http://localhost:8085/genres/index.html
public class GenresController {

    @GetMapping("")
    public String genresPage(){
        return "genres/index";
    }
}
